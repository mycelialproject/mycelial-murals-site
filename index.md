---
layout: default
title: Mycelial Mural
---

<p>&nbsp;</p>

The <b>Mycelial Mural</b> is a project between <a href="https://jadud.com/">Matt Jadud</a>, <a href="http://www.ericamott.com/">Erica Mott, Justice FIXME NAME, and <a href="http://hughsato.com/">Hugh Sato</a>. The mural presented at the Hyde Park Art Center is a "first release" of a living project, and wiull grow (virtually) as subsequent productions connect people and communities through art and stories about their place and experiences.


The first version of this open art project was designed and prototyped by Andrew Lowman, Julia Middlebrook, Julia Nash, and Jade Zhang as part of their work at Bates College in the course <a href="https://lynxruf.us/courses/dcs12s18/">DCS s12: Community Engaged Computing</a>, taught by Hamish Cameron and Matt Jadud. The mural is composed of work by students of <a href="https://www.linkedin.com/in/barbara-benjamin-mcmanus-5339b2b5">Barbara McManus</a> at the <a href="https://lms.lewistonpublicschools.org/">Lewiston Middle School</a> in Lewiston, Maine, and FIXME NAME at THE FIXME SCHOOL. Their work explores themes of justice, equity, and inclusion, and their thoughts are expressed through their photographic work as well as the stories they tell. 

If you want to recreate this work in your own community, feel free to use our process as documented here. And, please, reach out to us; we'd love to work with you, and connect your work through this site!

If you would like to learn more about the images in the mural, we invite you to explore the stories of the artists below.


<div class="divTable" style="width: 100%;" >
<div class="divTableBody">

<div class="divTableRow">
<div class="divTableCell"><b>Who</b></div>
<div class="divTableCell"><b>Where</b></div>
<div class="divTableCell"><b>When</b></div>
</div>

<div class="divTableRow">
<div class="divTableCell">&nbsp;</div>
<div class="divTableCell">&nbsp;</div>
<div class="divTableCell">&nbsp;</div>
</div>

<div class="divTableRow">
<div class="divTableCell"><a href="2018/LMS/">Lewiston Middle School</a></div>
<div class="divTableCell">Lewiston, ME</div>
<div class="divTableCell">May 2018</div>
</div>


</div>
</div>


